# Verbose Index for the NCEES FE Reference Handbook

## Objectives

- Create a numbered index to identify equations used in the [NCEES FE Reference Handbook][ncees_20180117_refbooks].

- Provide verbose commentary of equations used in the NCEES FE Reference Handbook in the form of text descriptions, alternate notations, and illustrations.

## Description

This repository contains numbered clarifying commentary of equations used in the NCEES FE Reference Handbook. The handbook is provided to examinees of the NCEES FE Exam. Equations in the NCEES handbook are unnumbered and typeset in order to pack as much content into each page as possible. In contrast, the purpose of this project is to provide an ad-hoc numbering system for the unnumbered equations in the handbook as well as to provide more verbose commentary of each equation than the space in the original handbook allows. The commentary may take the form of:

- Text descriptions.

- Alternate notations of equation symbols.

- Graphical illustrations.

- Relationships to other equations.

- Hyperlinks to websites hosting more detailed explanations.

The numbering system used to organize the Index is based upon the position of a referenced object (usually an equation) on a specific page within a specific version of the handbook. Position data includes as parameters:

- Page Number. The page number contained in the footer of a handbook page containing a referenced object.

- Column Number. The specific column of text where a referenced object lies.

- Line Number. The row of text. Numbers should be assigned without regard for text wrapping or equation grouping.

## Typesetting

- [GNU Texmacs](https://texmacs.org) is used for typesetting documents saved in the `.tm` file format.

- [Inkscape](https://inkscape.org/) is used for creating figures saved in `.svg` and `.eps` file formats.

## License

- Use of the order of equations included in the NCEES FE Reference Handbook are fair use of the copyrighted work by [NCEES](ncees.org).

- Descriptions, alternate notations, and illustrations are licensed under Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) by [Steven Baltakatei Sandoval](baltakatei.com) unless otherwise indicated.

[ncees_20180117_refbooks]: https://help.ncees.org/article/87-ncees-exam-reference-handbooks

[texmacs_2019_unixinstall]: http://www.texmacs.org/tmweb/download/unix.en.html
